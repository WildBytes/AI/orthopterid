import os
from multiprocessing import Pool
# import json
import requests
import urllib
# import ffmpeg
import numpy as np
# import argparse
from tqdm.auto import tqdm
import polars as pl

def download_xeno_canto_recordings(species, output_dir, max_nb_recordings=10):
    # Create the output directory if it does not exist
    os.makedirs(output_dir, exist_ok=True)
    # Get the list of recordings
    url = (f"https://www.xeno-canto.org/api/2/recordings?query={species}"
           f"&pg=1&per_page={max_nb_recordings}")
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    # Download the recordings
    for record in data["recordings"]:
        filename = f"{record['id']}.mp3"
        if not os.path.exists(os.path.join(output_dir, filename)):
            urllib.request.urlretrieve(record['file'], filename=os.path.join(output_dir, filename))


def make_dataset(all_species, output_dir):
    pool = Pool()
    pool.starmap(download_xeno_canto_recordings, [(species, os.path.join(output_dir, species)) for species in all_species])
    pool.close()

def nom_complet_to_species(nom_complet):
    species = nom_complet.split("</i>")[0]
    species = species.replace("<i>", "")
    return species

def main():
    df = pl.read_csv("data/Atlas des Orthoptères et Mantides de France métropolitaine_2024214.csv", separator=";")
    all_orthoptera = df.filter(df["ORDRE"] == "Orthoptera")
    all_species = all_orthoptera["NOM_COMPLET"].unique() 
    all_species = all_species.map_elements(nom_complet_to_species)
    all_species = all_species.to_list()
    output_dir = "data/dataset/"
    make_dataset(all_species, output_dir)


if __name__ == "__main__":
    main()


