# OrthopterID

OrthopterID is an attempt to implement a deep learning model able to identifiy orthoptera by sound.

## Setup

```bash
python3 -m venv .venv/orthopterid
source .venv/orthopterid/bin/activate
pip install -r requirements.txt
```

To install the Jupyter kernels:

```bash
python3 -m ipykernel install --name orthopterid --user
```

## Train

## Use


## Aknowledgements

We use [Xeno-Canto](https://www.xeno-canto.org/) Orthoptera records to train our model.


## Team

- Benamad Kader Houssein
- Samuel Ortion
